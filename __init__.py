# LFS Package Manager, copyright (C) 2020-2021 Les Fées Spéciales
# voeu@les-fees-speciales.coop
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.


bl_info = {
    "name": "LFS Package Manager",
    "author": "Les Fées Spéciales (LFS)",
    "version": (0, 3, 1.03),
    "blender": (2, 80, 0),
    "location": "User Preferences > LFS Package Manager",
    "tracker_url": "https://gitlab.com/lfs.coop/blender/lfs-blender-package-manager/-/issues",
    "description": "Update list of add-ons",
    "doc_url": "",
    "category": "Pipeline",
}

import bpy
from bpy.props import StringProperty, BoolProperty, EnumProperty, CollectionProperty
import addon_utils

import json
import os
import threading

from .logger import logger
from .package_downloader import GitLabPackageDownloader, GitHubPackageDownloader, packages, dep_dic
from .utils import get_request, config, get_install_path





DEBUG = False

is_dirty = False  # If a package was upgraded, display warning message


class ThreadPackageUpdateLight(threading.Thread):
    '''Threaded package list updater for when the destination is updated'''
    def __init__(self, package, install_path):
        threading.Thread.__init__(self)
        self.package = package
        self.install_path = install_path

    def run(self):
        updater = self.package['updater']
        updater.install_path = self.install_path
        self.package['is_upgradable'] = updater.is_upgradable()
        self.package['is_installed'] = updater.is_installed()


def destination_update(_self, _context):
    '''Update destination directory, where add-ons are stored'''
    install_path = get_install_path()
    threads = []

    for package in packages:
        t = ThreadPackageUpdateLight(package, install_path)
        threads.append(t)
        t.start()

    for t in threads:
        t.join()


# def update_department_dic():
#     '''Update the list of packages that are allowed to be installed'''
#     global dep_dic
#     packages_path = os.path.join(os.path.dirname(__file__), "department.json")
#     with open(packages_path, 'r') as f:
#         dep_dic = json.load(f)

def token_update(self, _context):
    '''Store tokens into the config file

    This allows tokens to persist even if the package manager is disabled.
    '''
    config_tokens = config.get('tokens', {})
    addon_prefs = bpy.context.preferences.addons[__name__].preferences
    for token in addon_prefs.tokens:
        config_tokens[token.name] = token.token
    config['tokens'] = config_tokens
    config.update_config()

class Token(bpy.types.PropertyGroup):
    token: StringProperty(name="Token",
                          description="Authentication token for private repository",
                          update=token_update)

allowed_package_keys = []
def update_allowed_packages_list(self, _context):
    '''Update the list of packages that are allowed to be installed'''
    global allowed_package_keys
    allowed_package_keys = []
    if self.department == 'ALL':
        allowed_package_keys = [package['name'] for package in packages]
    else:
        for package in packages:
            if package['name'] in dep_dic[self.department]:
                allowed_package_keys.append(package['name'])

class PackageUpgradePreferences(bpy.types.AddonPreferences):
    bl_idname = __name__

    auto_update: BoolProperty(
        name="Auto-update",
        description="Automatically update available packages",
        default=False)
    destination: EnumProperty(
        name="Destination",
        description="Where to install downloaded add-ons",
        items=(
            ('USER', 'User directory', 'Use the default Blender user directory'),
            ('PREFERENCES', 'From preferences',
             'Use the directory set in the scripts file paths in Blender’s user preferences'),
            ('CONFIG', 'From config', 'Use the directory in the JSON configuration.')
        ),
        default='USER',
        update=destination_update)
    tokens: CollectionProperty(
        name="Tokens",
        description="Tokens used for private repositories authentication",
        type=Token)
    filter: bpy.props.StringProperty(
        name='Package Filter',
        maxlen=1024,
        options={'TEXTEDIT_UPDATE'})
    department: bpy.props.StringProperty(
        name='Departement',
        description='',
        #if 'GPLYR_STROKE' exist use it as default else ''
        # default= lambda self [layer.info for layer in context.object.data.layers if layer.info.find('GPLYR_STROKE')!=-1 else ''][0] ],
        default = 'ALL',
        search=lambda self, context, edit_text: ['ALL']+[dep for dep in dep_dic.keys()],        
        update= update_allowed_packages_list
    )

    def draw(self, context):
        update_allowed_packages_list(self, context)
        layout = self.layout
        row = layout.row()
        row.prop(self, "auto_update")

        col = row.column(align=True)
        col.prop(self, "destination")
        col.label(text=get_install_path())
        
        row = layout.row()
        row.prop(self, "department")

        col = layout.column(align=True)
        col.operator("preferences.package_update_list", icon="FILE_REFRESH")
        upgrade_row = col.row(align=True)

        if is_dirty:
            col = layout.column(align=True)
            row = col.row()
            row.alignment='CENTER'
            row.alert = True
            row.label(text="Update complete. Please restart Blender!", icon='ERROR')

        # Packages
        show_upgrade_all = False
        if len(allowed_package_keys):
            col = layout.column(align=True)
            col.prop(self, "filter", text="", icon="VIEWZOOM")

            # Get categories for each package
            categories = {'': []}
            for package in packages:
                if package['name'] not in allowed_package_keys:
                    continue
                if not (self.filter.lower() in package['name'].lower()
                        or any(self.filter.lower() in c.lower()
                               for c in package['categories'])):
                    continue
                if package['categories']:
                    for category in package['categories']:
                        if category in categories:
                            categories[category].append(package)
                        else:
                            categories[category] = [package]
                else:
                    categories[''].append(package)
            if not categories['']:
                del categories['']

            for category_name in sorted(categories):
                category = categories[category_name]
                box = col.box()
                row = box.row()
                row.alignment = 'CENTER'
                if category_name != '':
                    row.label(text=category_name)

                split = box.split()
                name_col = split.column(align=True)
                action_col = split.column(align=True)

                blender_addons = [a.__name__ for a in addon_utils.modules(refresh=False)]
                prefs = context.preferences
                used_ext = {ext.module for ext in prefs.addons}

                for package in sorted(category, key=lambda p: p['name']):
                    name_split = name_col.split(factor=0.05, align=True)

                    module_name = package['activation']
                    is_enabled = module_name in used_ext
                    if module_name in blender_addons:
                        name_split.operator(
                            "preferences.addon_disable" if is_enabled else "preferences.addon_enable",
                            icon='CHECKBOX_HLT' if is_enabled else 'CHECKBOX_DEHLT', text="",
                            emboss=False,
                        ).module = module_name
                    else:
                        name_split.label(text="")
                    name_split.label(text=package['name'])

                    # if "pip_deps" in package:
                    #     row.label(text="Pip dependencies", icon="QUESTION")

                    row = action_col.row(align=True)
                    if 'updater' in package:
                        is_upgradable = package["is_upgradable"]
                        is_installed = (package['updater'].is_installed is True
                                        or callable(package['updater'].is_installed)
                                           and package['updater'].is_installed())
                        sub = row.row(align=True)
                        sub.active = is_upgradable == 'YES'
                        if is_upgradable == 'YES':
                            op = sub.operator("preferences.package_upgrade",
                                              icon="FILE_REFRESH" if is_installed
                                              else "IMPORT",
                                              text="Upgrade package" if is_installed
                                              else "Install package")
                            op.upgrade_all = False
                            show_upgrade_all = True
                        elif is_upgradable == 'NOT_FOUND':
                            op = sub.operator("preferences.package_upgrade", text="Package not found", icon='ERROR')
                            op.upgrade_all = False
                        elif is_upgradable == 'NOT_AVAILABLE':
                            op = sub.operator("preferences.package_upgrade", text="Package not available", icon='ERROR')
                            op.upgrade_all = False
                        else:
                            op = sub.operator("preferences.package_upgrade", text="Package up to date")
                            op.upgrade_all = False
                        op.package = package['name']
                        if is_installed:
                            op = row.operator("preferences.package_remove", text="", icon="TRASH")
                            op.package = package['name']
                            if "installation" in package and package['installation']:
                                op = row.operator("preferences.addon_show", icon="SETTINGS")
                                op.module = package['activation']
                    else:
                        row.alert = True
                        row.label(text="Please update package list")

            upgrade_row.active = show_upgrade_all
            if show_upgrade_all:
                op = upgrade_row.operator("preferences.package_upgrade", text="Upgrade all packages", icon="FILE_REFRESH")
            else:
                op = upgrade_row.operator("preferences.package_upgrade", text="All packages up to date")
            op.upgrade_all = True

        # Tokens
        col = layout.column()
        if len(self.tokens):
            box = col.box()
            box.label(text="Tokens")
            split = box.split()
            name_col = split.column(align=True)
            token_col = split.column(align=True)
            for token in self.tokens:
                name_col.label(text=token.name)
                token_col.prop(token, "token")


def update_package_list(op):
    """Update package list from server"""
    addon_prefs = bpy.context.preferences.addons[__name__].preferences
    logger.info("Updating packages...")

    content = get_request(config['package_list_url'])
    
    if content is None:
        return
    packages.clear()
    if DEBUG:
        # Local package list
        packages_path = os.path.join(os.path.dirname(__file__), "packages.json")
        with open(packages_path, 'r') as f:
            packages.extend(json.load(f))
    else:        
        packages.extend(json.loads(content))
        
    logger.info("Done updating packages.")
    
    logger.info("Updating department lists...")
    dep_list = get_request(config['department_list_url'])
    global dep_dic
    dep_dic.clear()
    if DEBUG:
        # Local package list
        dept_path = os.path.join(os.path.dirname(__file__), "department.json")
        with open(dept_path, 'r') as f:
           dep_dic = json.load(f)
    else:
        try:
            dep_dic = json.loads(dep_list)
        except Exception as e:
            logger.error("Error while loading package list: %s" % e)
            

    logger.info("Done updating  department lists.")

    if addon_prefs.auto_update:
        check_package_updates()
        update_allowed_packages_list(addon_prefs, None)
        upgrade_packages(op, None, True)
        logger.info("Done upgrading packages.")


class ThreadPackageUpdate(threading.Thread):
    def __init__(self, package, logger, install_path, addon_prefs):
        threading.Thread.__init__(self)
        self.package = package
        self.logger = logger
        self.install_path = install_path
        self.addon_prefs = addon_prefs

    def run(self):
        self.logger.info("Checking %s..." % self.package['name'])
        service = self.package.get("service", None)
        if service == 'gitlab':
            updater = GitLabPackageDownloader(self.package, self.install_path)
        elif service == 'github':
            updater = GitHubPackageDownloader(self.package, self.install_path)
        else:
            self.logger.error("Service %s not found" % service)
            return

        self.package['updater'] = updater
        self.package['is_upgradable'] = updater.is_upgradable()
        self.package['is_installed'] = updater.is_installed()

        if "private_token" in self.package:
            token = self.package['private_token']
            if not token in self.addon_prefs.tokens:
                pref_token = self.addon_prefs.tokens.add()
                pref_token.name = token


def check_package_updates():
    """Check for each package if it can be updated"""
    install_path = get_install_path()
    os.makedirs(install_path, exist_ok=True)
    addon_prefs = bpy.context.preferences.addons[__name__].preferences

    logger.info("Checking packages' upgradability.")
    threads = []
    for package in packages:
        t = ThreadPackageUpdate(package, logger, install_path, addon_prefs)
        threads.append(t)
        t.start()

    for t in threads:
        t.join()

    logger.info("Done checking packages' upgradability.")


class PREFERENCES_OT_Package_Update_List(bpy.types.Operator):
    """Update list of updatable packages"""
    bl_idname = "preferences.package_update_list"
    bl_label = "Update package list"

    def execute(self, context):
        # update_department_dic()
        update_package_list(self)
        check_package_updates()
        return {'FINISHED'}


def upgrade_packages(op, package_name, upgrade_all):
    updater=None
    for package in packages:
        if(( not upgrade_all and package['name'] != package_name)
            or (allowed_package_keys is not None and package['name'] not in allowed_package_keys)):
            continue
        updater = package['updater']
        if package['is_upgradable'] == 'YES':
            logger.info("Upgrading %s..." % package['name'])
            updater.update_package()
            # Set dirty flag, to display warning if package updated
            if package['project'] == __name__ or 'pip_deps' in package:
                global is_dirty
                is_dirty = True
                if op is not None:
                    op.report({'WARNING'}, "Update complete. Please restart Blender!")
        else:
            logger.info("Package %s not upgraded" % package['name'])

    # Activate packages
    addon_utils.modules_refresh()
    if updater is not None and updater.activation:
        for package in packages:
            if not upgrade_all and package['name'] != package_name:
                continue
            updater = package['updater']
            if package['is_upgradable'] == 'YES':
                addon_utils.enable(updater.activation, default_set=True,
                                   persistent=True)


class PREFERENCES_OT_Package_Upgrade(bpy.types.Operator):
    """Upgrade single package"""
    bl_idname = "preferences.package_upgrade"
    bl_label = "Upgrade package"

    package: StringProperty()
    upgrade_all: BoolProperty(default = False)

    def execute(self, context):
        upgrade_packages(self, self.package, self.upgrade_all)
        return {'FINISHED'}


class PREFERENCES_OT_Package_Remove(bpy.types.Operator):
    """Remove package"""
    bl_idname = "preferences.package_remove"
    bl_label = "Remove package"

    package: StringProperty()

    def invoke(self, context, event):
        return context.window_manager.invoke_confirm(self, event)

    def execute(self, context):
        for package in packages:
            if package['name'] == self.package:
                break
        logger.info("Removing package %s..." % self.package)

        package['updater'].remove()

        package['is_installed'] = False
        package['is_upgradable'] = 'YES'
        addon_utils.modules_refresh()

        return {'FINISHED'}


# REGISTER
def register():
    bpy.utils.register_class(PREFERENCES_OT_Package_Update_List)
    bpy.utils.register_class(PREFERENCES_OT_Package_Upgrade)
    bpy.utils.register_class(PREFERENCES_OT_Package_Remove)
    bpy.utils.register_class(Token)
    bpy.utils.register_class(PackageUpgradePreferences)
    
    update_package_list(None)
    # update_department_dic()

    # Refresh list of tokens from config file
    addon_prefs = bpy.context.preferences.addons[__name__].preferences
    for token_name in dict(config.get('tokens', {})):
        if token_name in addon_prefs.tokens:
            prefs_token = addon_prefs.tokens.get(token_name)
        else:
            prefs_token = addon_prefs.tokens.add()
        prefs_token.name = token_name
        prefs_token.token = config['tokens'][token_name]
    if "tokens" in config and "" in config['tokens']:
        config['tokens'].pop("")


def unregister():
    bpy.utils.unregister_class(PREFERENCES_OT_Package_Update_List)
    bpy.utils.unregister_class(PREFERENCES_OT_Package_Upgrade)
    bpy.utils.unregister_class(PREFERENCES_OT_Package_Remove)
    bpy.utils.unregister_class(PackageUpgradePreferences)
    bpy.utils.unregister_class(Token)
