# LFS Package Manager, copyright (C) 2020-2021 Les Fées Spéciales
# voeu@les-fees-speciales.coop
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.


import os
import json
import bpy
import re
from urllib.parse import quote_plus
import zipfile
import sys
from tempfile import TemporaryFile
import subprocess

from .logger import logger
from .utils import get_request, config, local_info

packages = []
dep_dic = {}

class PackageDownloader():
    """Download a package from a remote repository."""
    def __init__(self, package, install_path):
        self.package_name = package['name']
        for k, v in package.items():
            setattr(self, k, v)
        self.install_path = install_path

    def remove(self):
        bpy.ops.preferences.addon_remove(module=self.activation)
        self.del_local_info()

    def set_local_info(self, installed_files, installed_pip_modules):
        """Keep a record of info for this file. Next time, don’t download it if
        identical. This includes at present the latest ref (hash), and a list
        of downloaded files. """
        package_info = {}
        package_info['latest_ref'] = self.ref
        package_info['installed_files'] = list(installed_files)
        package_info['installed_pip_modules'] = list(installed_pip_modules)
        local_info[self.package_name] = package_info

    def del_local_info(self):
        """Delete local info from record."""
        if self.package_name in local_info:
            del local_info[self.package_name]

    def get_latest_remote_commit(self):
        raise NotImplementedError

    def is_installed(self):
        """Check whether the package is present in the list of local refs."""
        return self.package_name in local_info

    def is_upgradable(self):
        """Check whether the package to get has changed since last update."""
        # Ignore package if not available on current platform
        import platform
        if hasattr(self, "platform") and platform.system() not in self.platform:
            return 'NOT_AVAILABLE'

        if self.ref_type == "branch":
            # Get latest commit hash in that branch, via API, compare to local commit
            latest_remote_ref = self.get_latest_remote_commit()
        elif self.ref_type == "commit":
            latest_remote_ref = self.ref
        else:
            logger.error("Unknown ref type")
            raise TypeError("Unknown ref type")

        # Package is not accessible?
        if latest_remote_ref is None:
            return 'NOT_FOUND'

        if self.package_name in local_info:
            # Versioning code: local info used to be a dict of ref strings {"package": "ref"},
            # but is now a dict of package info dicts {"package": {...}}
            if type(local_info[self.package_name]) == str:
                upgradable = latest_remote_ref != local_info[self.package_name]
            else:
                # If any file was deleted, mark it updatable
                if 'installed_files' in local_info[self.package_name]:
                    for filepath in local_info[self.package_name]['installed_files']:
                        if not os.path.exists(filepath):
                            return 'YES'
                upgradable = latest_remote_ref != local_info[self.package_name]['latest_ref']
            return 'YES' if upgradable else 'NO'
        else:
            logger.info("Ref not yet in local info")
            return 'YES'

    def update_package(self):
        dl_url = self.get_dl_url()

        # On update, remove package if already installed
        # This just returns {'CANCELLED'} if add-on could not be found
        try:
            bpy.ops.preferences.addon_remove(module=self.activation)
        except AttributeError:
            pass

        with TemporaryFile() as temp_zip:
            # Download zip as temp file
            content = get_request(dl_url)
            temp_zip.write(content)
            installed_files = set()
            installed_pip_modules = set()
            with zipfile.ZipFile(temp_zip) as zip_file:
                # Extract zip file
                if self.files:
                    name_list = zip_file.namelist()
                    for dst_dir, patterns in self.files.items():
                        # Dictionary of {destination: [pattern, pattern, ...], ...}
                        files_to_extract = set()
                        for pattern in patterns:
                            pattern = pattern.replace("*", "[^/]*")
                            for name in name_list:
                                if re.match(pattern, name):
                                    files_to_extract.add(name)
                        for zip_file_name in files_to_extract:
                            logger.info("Installing %s..." % zip_file_name)
                            with zip_file.open(zip_file_name) as src_file:
                                dst_path = zip_file_name.replace("\\", "/")
                                dst_name = dst_path.split("/")[-1]

                                dst_path = os.path.join(self.install_path, dst_dir, dst_name)
                                os.makedirs(os.path.dirname(dst_path), exist_ok=True)
                                with open(dst_path, 'wb') as dst_file:
                                    dst_file.write(src_file.read())
                                installed_files.add(dst_path)
                else:
                    logger.error("No file found for package %s." % self.package_name)

        if hasattr(self, "pip_deps"):
            for pip_dep in self.pip_deps:
                if not self.install_pip_dep(pip_dep):
                    logger.error("Could not install pip dep %s. Can you modify the Blender directory?" % pip_dep)
                else:
                    installed_pip_modules.add(pip_dep)

        self.set_local_info(installed_files, installed_pip_modules)

        for list_package in packages:
            if list_package['updater'] == self:
                break
        list_package['is_upgradable'] = self.is_upgradable()
        list_package['is_installed'] = self.is_installed()

    def install_pip_dep(self, module_name):
        python_path = sys.executable
        subp = subprocess.run([python_path, "-m", "ensurepip"])
        if subp.returncode != 0:
            return False
        subp = subprocess.run([python_path, "-m", "pip", "install",
                               "--target=" + os.path.join(self.install_path, "modules"),
                               module_name, "--upgrade"])
        if subp.returncode != 0:
            return False
        return True


class GitLabPackageDownloader(PackageDownloader):
    def get_dl_url(self):
        project_name = quote_plus("%s/%s" % (self.group, self.project))
        instance = getattr(self, "instance", "gitlab.com")
        api_url = "https://{instance}/api/v4/projects/{project}/repository/archive.zip?sha={ref}".format(
            instance=instance, group=self.group, project=project_name, ref=self.ref)
        private_token = hasattr(self, "private_token") and self.private_token
        if private_token:
            addon_prefs = bpy.context.preferences.addons[__name__.split('.')[0]].preferences
            if private_token in addon_prefs.tokens and addon_prefs.tokens[private_token]:
                api_url += '&private_token=' + addon_prefs.tokens[private_token].token

        return api_url

    def get_latest_remote_commit(self):
        project_name = quote_plus("%s/%s" % (self.group, self.project))
        instance = getattr(self, "instance", "gitlab.com")
        api_url = "https://{instance}/api/v4/projects/{project}/repository/branches/".format(
            instance=instance, project=project_name)
        private_token = hasattr(self, "private_token") and self.private_token
        if private_token:
            addon_prefs = bpy.context.preferences.addons[__name__.split('.')[0]].preferences
            if private_token in addon_prefs.tokens and addon_prefs.tokens[private_token]:
                api_url += '?private_token=' + addon_prefs.tokens[private_token].token

        try:
            response = json.loads(get_request(api_url))
        except TypeError:
            logger.error("Could not access repository %s info" % self.project)
            return None

        # Check that repo is accessible
        try:
            for branch in response:
                if self.ref == branch["name"]:
                    self.ref = branch["commit"]["id"]
                    return branch["commit"]["id"]
        except TypeError:
            logger.error("Could not access repository %s info" % self.project)
            return None
        return self.ref


class GitHubPackageDownloader(PackageDownloader):
    def get_dl_url(self):
        return "https://github.com/{group}/{project}/archive/{ref}.zip".format(
            group=self.group, project=self.project, ref=self.ref)

    def get_latest_remote_commit(self):
        api_url = "https://api.github.com/projects/{group}/{project}/git/ref/heads/{ref}".format(group=self.group,
                                                                                                 project=self.project,
                                                                                                 ref=self.ref)
        response = json.loads(get_request(api_url))
        if "object" in response:
            self.ref = response["object"]["sha"]
        return self.ref
